
macro( NETRUSH_USE_BOOST )

    # Useful options of Boost.
    add_definitions(
        -DBOOST_THREAD_VERSION=4             # Force Boost.Thread V4 use for all projects, including dependencies.
        -DBOOST_RESULT_OF_USE_DECLTYPE       # TODO: review this
        -DBOOST_THREAD_PROVIDES_NESTED_LOCKS # TODO: review this
        -DBOOST_THREAD_USES_DATETIME         # TODO: Why the hell did I do this?
    )

    set( Boost_USE_STATIC_LIBS        ON )
    set( Boost_USE_MULTITHREADED      ON )
    set( Boost_USE_STATIC_RUNTIME    OFF )

    find_package( Boost 1.67.0 EXACT REQUIRED COMPONENTS ${ARGV} )

    if( NOT Boost_FOUND )
        message( FATAL_ERROR "Boost libraries NOT FOUND!" )
    endif()

    include_directories( ${Boost_INCLUDE_DIR} )

endmacro()

